const Router = require('express')
const router = new Router()

const userRouter = require('./userRouter')
const profileRouter = require('./profileRouter')
const commentsRouter = require('./commentsRouter')
const filesRouter = require('./filesRouter')



router.use('/comments', commentsRouter)
router.use('/file', filesRouter)
router.use('/user', userRouter)
router.use('/profile', profileRouter)


module.exports = router
