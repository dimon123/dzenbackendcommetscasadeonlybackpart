const sequelize = require('../db')
const {DataTypes} = require('sequelize')

const User = sequelize.define('user', {
    id: {type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true},
    email: {type: DataTypes.STRING, unique: true, allowNull: false},
    password: {type: DataTypes.STRING, allowNull: false},
    role: {type: DataTypes.STRING, defaultValue: "USER"},
})

//experemental shit
 
const Basket = sequelize.define('basket', {
    id: {type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true},
    userId: {type: DataTypes.INTEGER}
})
//


const UserProfile = sequelize.define('userProfile', {
    id: {type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true},
    userid: {type: DataTypes.INTEGER, allowNull: false, unique: true  },
    name : {type: DataTypes.STRING, allowNull: false },
    description : {type: DataTypes.STRING},
    hobbys : {type: DataTypes.STRING},
    country : {type: DataTypes.STRING, allowNull: false},
    city : {type: DataTypes.STRING, allowNull: false},
    age : {type: DataTypes.INTEGER},
    img : {type: DataTypes.STRING},
})

const Comments  = sequelize.define('comments', {
    id :{type:DataTypes.INTEGER, primaryKey:true, autoIncrement:true},
    parantId :{type:DataTypes.INTEGER, allowNull:false}, // id сообщения на которое отвечают если просто то 0
    username:{type:DataTypes.STRING, allowNull:false},
    email:{type:DataTypes.STRING, allowNull:false},
    homePage:{type:DataTypes.STRING}, //какой то юрл из тз, не обязательное поле
    text:{type:DataTypes.STRING, allowNull:false},
    userImg :{type:DataTypes.STRING}, //аватарка
    userFile :{type:DataTypes.STRING} //файл или картинка
} )



UserProfile.hasOne(User)
User.belongsTo(UserProfile)

UserProfile.hasMany(Comments)
Comments.belongsTo(UserProfile)



module.exports = {
    User,
    UserProfile,
    Basket,
    Comments
}





