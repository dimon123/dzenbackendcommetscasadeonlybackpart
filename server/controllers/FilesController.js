const fs = require('fs')
const path = require('path');
const uid = require('uid');

const uploadDir = '../uploads'

class FilesController {
    
    async create(req, res) {
        try {
          const file = req.files.file;

          let filePath = path.join(uploadDir, `uniqueFileName${Math.random(100000)}.png`);

          if (fs.existsSync(filePath)) {
              return res.status(400).json({message: 'File already exist'})
          }

          file.mv(filePath);

          res.json(`${uploadDir}/${filePath}`);
        } catch (e) {
            console.log(e)
            return res.status(500).json(e)
        }
    }
}

module.exports = new FilesController()
